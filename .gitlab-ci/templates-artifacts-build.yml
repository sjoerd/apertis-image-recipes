.pipeline_version_snippet: &pipeline_version
  - PIPELINE_VERSION=$(cat a/*/meta/build-id.txt)

######
# stage: ostree rollbackbundle build

.ostree-rollbackbundle-build:
  stage: ostree rollbackbundle build
  variables:
    branch: ${osname}/${release}/${architecture}-${board}/${type}
    rollback_branch: ${osname}/${release}/${architecture}-${board}/${type}-rollback
    repo: repo-${architecture}-${board}-${type}/
  script:
    - *pipeline_version
    - image_name=${osname}_ostree_${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION}
    - cd a/${PIPELINE_VERSION}/${architecture}/${type}
    - echo "/usr/bin/updatectl" > skip_list
    - ostree --repo=${repo} commit
        --tree=ref=${branch}
        --branch=${rollback_branch}
        --add-metadata-string=ostree.collection-binding=${collection_id}
        --bind-ref=${branch}
        --skip-list="skip_list"
    - rm skip_list
    - COMMIT=$(ostree --repo=$repo rev-parse $rollback_branch)
    - ostree --repo=$repo sign --sign-type=ed25519 --keys-file=${OSTREE_SIGNING_KEY} $COMMIT
    - mkdir -p tests
    - ostree --repo=${repo} static-delta generate
        --from=${branch}
        --to=${rollback_branch}
        --inline
        --min-fallback-size=1024
        --filename tests/${image_name}_rollback.delta
  artifacts:
    expire_in: 1d
    paths:
      - a/*/*/*/tests/*_rollback.delta

######
# stage: ostree bundle build

.ostree-bundle-build:
  stage: ostree bundle build
  variables:
    branch: ${osname}/${release}/${architecture}-${board}/${type}
    repo: repo-${architecture}-${board}-${type}/
  script:
    - *pipeline_version
    - image_name=${osname}_ostree_${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION}
    - cd a/${PIPELINE_VERSION}/${architecture}/${type}
    - ostree --repo=${repo} static-delta generate
        --empty
        --to=${branch}
        --inline
        --min-fallback-size=1024
        --filename ${image_name}.delta
  artifacts:
    expire_in: 1d
    paths:
      - a/*/*/*/*.delta

######
# stage: ostree image build

.ostree-image-build:
  stage: ostree image build
  tags:
    - aws
  variables:
    repo: repo-${architecture}-${board}-${type}/
    ostree_pull_url: ${image_url_prefix}/${ostree_path}
  script:
    - *pipeline_version
    - image_name=${osname}_ostree_${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION}
    - cd a/${PIPELINE_VERSION}/${architecture}/${type}
    - debos ${debosarguments}
        --show-boot
        -t architecture:${architecture}
        -t type:$type
        -t board:$board
        -t suite:$release
        -t ospack:ospack_${release}-${architecture}-${type}_${PIPELINE_VERSION}
        -t image:${image_name}
        -t message:${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION}
        -t ostree:${repo}
        -t repourl:${ostree_pull_url}
        -t collection_id:${collection_id}
        ${WORKSPACE}/apertis-ostree-image-${board}.yaml
  artifacts:
    expire_in: 1d
    paths:
      - a/*/*/*/*.img.*

######
# stage: apt image build

.apt-image-build:
  stage: apt image build
  tags:
    - aws
  script:
    - *pipeline_version
    - image_name=${osname}_${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION}
    - cd a/${PIPELINE_VERSION}/${architecture}/${type}
    - debos ${debosarguments}
        --show-boot
        -t architecture:${architecture}
        -t type:${type}
        -t ospack:ospack_${release}-${architecture}-${type}_${PIPELINE_VERSION}
        -t suite:$release
        -t timestamp:${PIPELINE_VERSION}
        -t image:${image_name}
        ${WORKSPACE}/${osname}-image-${board}.yaml
  artifacts:
    expire_in: 1d
    paths:
      - a/*/*/*/*.img.*

######
# stage: installer image build

.installer-image-build:
  stage: installer image build
  tags:
    - aws
  script:
    - *pipeline_version
    - mkdir -p a/${PIPELINE_VERSION}/installer/${target}
    - cd a/${PIPELINE_VERSION}/installer/${target}
    - debos
        --show-boot
        -t suite:${release}
        ${WORKSPACE}/${target}-installer.yaml
  artifacts:
    expire_in: 1d
    paths:
      - a/*/*/*/*.img.*
  needs:
    - debos-dry-run
    - compute-version

######
# stage: sysroot image build

.sysroot-build:
  stage: sysroot build
  tags:
    - aws
  script:
    - *pipeline_version
    - sysrootname="sysroot-${osname}-${release}-${architecture}-${PIPELINE_VERSION}"
    - sysrooturl="${image_url_prefix}/daily/${release}/${PIPELINE_VERSION}/${architecture}/sysroot/${sysrootname}.tar.gz"
    - metadata_file="sysroot/${release}/sysroot-${osname}-${release}-${architecture}"
    - metadata_contents="version=${release} ${PIPELINE_VERSION}\nurl=${sysrooturl}\n"
    - mkdir -p "a/sysroot/${release}"
    - echo "${metadata_contents}" | tee "a/${metadata_file}"
    - cd a/${PIPELINE_VERSION}/${architecture}/${type}
    - debos ${debosarguments}
        --show-boot
        -t architecture:${architecture}
        -t ospack:ospack_${release}-${architecture}-${type}_${PIPELINE_VERSION}
        -t sysroot:${sysrootname}
        ${WORKSPACE}/${osname}-sysroot.yaml
  artifacts:
    expire_in: 1d
    paths:
      - a/*/*/*/sysroot-*
      - a/sysroot/*/sysroot-*
