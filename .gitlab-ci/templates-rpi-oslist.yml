.oslist-generate-snippet: &oslist-generate-snippet
    - |
      if [ "${VARIANT}" = "ostree" ]; then
          EXTRACTED_NAME="${osname}_ostree_${image_suffix}.img"
          SHORTDESC="OSTree "
          LONGDESC="OSTree-based "
      else
          EXTRACTED_NAME="${osname}_${image_suffix}.img"
          SHORTDESC=""
          LONGDESC=""
      fi
    - IMAGE_NAME="${EXTRACTED_NAME}.gz"
    - test -f "${image_dir}/${IMAGE_NAME}" || wget -O "${image_dir}/${IMAGE_NAME}" "${image_url}/${IMAGE_NAME}"
    - zcat "${image_dir}/${IMAGE_NAME}" > "${image_dir}/${EXTRACTED_NAME}"
    - |
      echo "${ENTRY_TEMPLATE}" | jq \
          --arg name "Apertis ${SHORTDESC}${type} image (daily build)" \
          --arg description "Apertis ${LONGDESC}${type} image for Raspberry Pi 3/4 (daily build)" \
          --arg url "${image_url}/${IMAGE_NAME}" \
          --argjson extract_size $(stat -c %s ${image_dir}/${EXTRACTED_NAME}) \
          --arg extract_sha256 "$(sha256sum ${image_dir}/${EXTRACTED_NAME} | cut -f 1 -d ' ')" \
          --argjson image_download_size $(stat -c %s ${image_dir}/${IMAGE_NAME}) \
          --arg image_download_sha256 "$(sha256sum ${image_dir}/${IMAGE_NAME} | cut -f 1 -d ' ')" \
          --arg release_date "$(date -d ${PIPELINE_VERSION%.*} +%F)" \
          '. | .name=$name | .description=$description | .url=$url | .extract_size=$extract_size | .extract_sha256=$extract_sha256 | .image_download_size=$image_download_size | .image_download_sha256=$image_download_sha256 | .release_date=$release_date' \
          > a/${PIPELINE_VERSION}/rpi/oslist-entry-${type}-${VARIANT}.json

.oslist-generate:
  stage: oslist build
  variables:
    image_dir: a/${PIPELINE_VERSION}/${architecture}/${type}
    image_suffix: ${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION}
    image_url: ${IMAGE_URL_PREFIX}/daily/${release}/${PIPELINE_VERSION}/${architecture}/${type}
  script:
    # Gitlab CI doesn't like string literals containing several double-quotes and a colon,
    # so we use a variable to work around this issue
    - COLON=":"
    - FLD_NAME="\"name\"${COLON} \"\""
    - FLD_DESC="\"description\"${COLON} \"\""
    - FLD_URL="\"url\"${COLON} \"\""
    - FLD_ICON="\"icon\"${COLON} \"https://images.apertis.org/rpi/apertis-logo.png\""
    - FLD_ESIZE="\"extract_size\"${COLON} 0"
    - FLD_ESHA="\"extract_sha256\"${COLON} \"\""
    - FLD_ISIZE="\"image_download_size\"${COLON} 0"
    - FLD_ISHA="\"image_download_sha256\"${COLON} \"\""
    - FLD_DATE="\"release_date\"${COLON} \"\""
    - ENTRY_TEMPLATE="{${FLD_NAME}, ${FLD_DESC}, ${FLD_URL}, ${FLD_ICON}, ${FLD_ESIZE}, ${FLD_ESHA}, ${FLD_ISIZE}, ${FLD_ISHA}, ${FLD_DATE}}"
    - mkdir -p ${image_dir} a/${PIPELINE_VERSION}/rpi
    # Build entry for apt image
    - VARIANT="apt"
    - *oslist-generate-snippet
    # Build entry for ostree image
    - VARIANT="ostree"
    - *oslist-generate-snippet
  artifacts:
    expire_in: 1d
    paths:
      - a/*/rpi/*.json

.oslist-upload:
  variables:
    target: rpi
    upload_dest: ${upload_host}:${UPLOAD_ROOT}
    source: a/${PIPELINE_VERSION}/rpi/
  before_script:
    # Ensure we have a master list and the logo image file
    - cp ${CI_PROJECT_DIR}/.gitlab-ci/assets/* a/${PIPELINE_VERSION}/rpi/
    - echo '{"os_list":[]}' > "a/${PIPELINE_VERSION}/rpi/apertis-oslist-daily.json"
    - POS=0
    - |
      for file in a/${PIPELINE_VERSION}/rpi/oslist-entry-*.json; do
          ENTRY="$(cat $file)"
          cat "a/${PIPELINE_VERSION}/rpi/apertis-oslist-daily.json" | jq \
              --argjson entry "${ENTRY}" \
              ". | .os_list[${POS}]=\$entry" \
              > tmp.json
          mv tmp.json "a/${PIPELINE_VERSION}/rpi/apertis-oslist-daily.json"
          rm "$file"
          POS=$(($POS+1))
      done
