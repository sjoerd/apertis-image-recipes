echo "Copying U-Boot to RAM"
load mmc 0:1 0x12000000 /deb-binaries/usr/lib/u-boot/mx6qsabrelite/u-boot.imx
echo "Probing SPI-NOR"
sf probe 0
echo "Erasing SPI_NOR"
sf erase 0x0 0xc0000
echo "Writing U-Boot to SPI-NOR"
sf write 0x12000000 0x400 0xc0000
echo ""
echo '+-----------------------------------------------------------------+'
echo '|                  U-Boot installation complete                   |'
echo '|                                                                 |'
echo '| Please remove the SD Card and power cycle the board to continue |'
echo '+-----------------------------------------------------------------+'
loop 0 0
