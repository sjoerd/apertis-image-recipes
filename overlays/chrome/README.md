Dummy directory to allow recipes to work for testing without fetching chrome.

In CI (or manually for testing) this directory should be populated via:
* Running the fetch-chrome scrript
* Checking out the ChromeController repository
