{{ $architecture := or .architecture "arm64" }}
{{ $type := or .type "minimal" }}
{{ $suite := or .suite "v2021dev1" }}
{{ $image := or .image (printf "apertis-ostree-%s-%s-%s" $suite  $type $architecture) }}

{{ $board := or .board "uboot" }}
{{ $repourl := or .repourl "https://images.apertis.org/ostree/repo" }}
{{ $osname := or .osname "apertis" }}
{{ $branch := or .branch (printf "%s/%s/%s-%s/%s" $osname $suite $architecture $board $type) }}
{{ $ostree := or .ostree "repo" }}

{{ $cmdline := or .cmdline "rootwait rw quiet splash plymouth.ignore-serial-consoles fsck.mode=auto fsck.repair=yes" }}

{{ $demopack := or .demopack "disabled" }}

architecture: {{ $architecture }}

actions:
  - action: image-partition
    imagename: {{ $image }}.img
{{ if eq $type "minimal" }}
    imagesize: 4G
{{end}}
{{ if eq $type "target" "development" }}
    imagesize: 15G
{{end}}
    partitiontype: gpt

    mountpoints:
      - mountpoint: /
        partition: system
      - mountpoint: /boot
        partition: boot
      - mountpoint: /home
        partition: general_storage

    partitions:
      - name: boot
        fs: ext2
        start: 0%
        end: 1024M
        flags: [ boot ]
        options: [ x-systemd.automount ]
      - name: system
        fs: ext4
        start: 1024M
        end: 3000M
      - name: general_storage
        fs: ext4
        start: 3000M
        end: 100%

  - action: run
    description: Install bootloader
    chroot: false
    script: scripts/setup-uboot-bootloader.sh

  - action: ostree-deploy
    description: Deploying ostree onto image
    repository: {{ $ostree }}
    remote_repository: {{ $repourl }}
    branch: {{ $branch }}
    os: {{ $osname }}
    append-kernel-cmdline: {{ $cmdline }}
    {{ if .collection_id }}
    collection-id: {{ .collection_id }}
    {{ end }}

  - action: run
    description: "Enable signature verification"
    chroot: false
    command: ostree --repo="${ROOTDIR}/ostree/repo" config set 'remote "origin"'.sign-verify "true"

  # Add multimedia demo pack
  # Provide URL via '-t demopack:"https://images.apertis.org/media/multimedia-demo.tar.gz"'
  # to add multimedia demo files
  {{ if ne $demopack "disabled" }}
  # Use wget to get some insight about https://phabricator.collabora.com/T11930
  # TODO: Revert to a download action once the cause is found
  - action: run
    description: Download multimedia demo pack
    chroot: false
    command: wget --debug {{ $demopack }} -O "${ARTIFACTDIR}/multimedia-demo.tar.gz"

  - action: unpack
    description: Unpack multimedia demo pack
    compression: gz
    file: multimedia-demo.tar.gz

  - action: run
    description: Clean up multimedia demo pack tarball
    chroot: false
    command: rm "${ARTIFACTDIR}/multimedia-demo.tar.gz"
  {{ end }}

  - action: run
    description: List files on {{ $image }}
    chroot: false
    script: scripts/list-files "$ROOTDIR" | gzip > "${ARTIFACTDIR}/{{ $image }}.img.filelist.gz"

  - action: run
    description: Create block map for {{ $image }}.img
    postprocess: true
    command: bmaptool create {{ $image }}.img > {{ $image }}.img.bmap

  - action: run
    description: Compress {{ $image }}.img
    postprocess: true
    command: gzip -f {{ $image }}.img

  - action: run
    description: Checksum for {{ $image }}.img.gz
    postprocess: true
    command: sha256sum {{ $image }}.img.gz > {{ $image }}.img.gz.sha256
