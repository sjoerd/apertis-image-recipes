import org.junit.*
import com.lesfurets.jenkins.unit.*
import static groovy.test.GroovyAssert.*

class DockerMock {
    PipelineTestHelper helper

    DockerMock(PipelineTestHelper h) {
        helper = h
    }

    void withRegistry(String registry, Closure c) {
        helper.callClosure(c)
    }

    DockerImageMock image(String) {
        return new DockerImageMock(helper)
    }
}

class DockerImageMock {
    PipelineTestHelper helper

    DockerImageMock(PipelineTestHelper h) {
        helper = h
    }

    void pull() {
    }

    void inside(String args, Closure c) {
        helper.callClosure(c)
    }
}

class EnvMock extends LinkedHashMap {
    Binding binding

    EnvMock(Binding b) {
        binding = b
    }

    @Override
    Object put(Object key, Object value) {
        super.put(key, value)
        binding.setVariable(key, value)
    }
}

class CurrentBuildMock extends LinkedHashMap {
    CurrentBuildMock() {
        put('result', 'SUCCESS')
    }

    boolean resultIsBetterOrEqualTo(String s) {
        if (get('results') == 'SUCCESS')
            return true
        if (get('results') == 'UNSTABLE')
            return s in ['UNSTABLE', 'SUCCESS']
        return false
    }
}

class JenkinsfileTest extends BasePipelineTest {
    static def withCredentialsInterceptor = { list, closure ->
        list.forEach {
            def vars = it instanceof List ? it : [ it ]
            vars.forEach {
                    binding.setVariable(it, "$it")
                }
        }
        def res = closure.call()
        list.forEach {
            def vars = it instanceof List ? it : [ it ]
            vars.forEach {
                    binding.setVariable(it, null)
                }
        }
        return res
    }

    @Override
    @Before
    void setUp() throws Exception {
        super.setUp()
        helper.registerAllowedMethod("VersionNumber", [Map.class], {params ->
            assert params.versionNumberString == '${BUILD_DATE_FORMATTED,"yyyyMMdd"}.${BUILDS_TODAY_Z}'
            return "20190401.0"
        })
        helper.registerAllowedMethod("withCredentials", [ List.class, Closure.class], withCredentialsInterceptor)
        helper.registerAllowedMethod("parameters", [ArrayList.class], null)
        helper.registerAllowedMethod("writeFile", [Map.class], null)
        helper.registerAllowedMethod("file", [Map.class], {m -> m.variable})
        helper.registerAllowedMethod("usernamePassword", [Map.class], {m -> [m.usernameVariable, m.passwordVariable]})
        helper.registerAllowedMethod("sshagent", [Map.class, Closure.class], null)
        helper.registerAllowedMethod("deleteDir", [], null)
        helper.registerAllowedMethod("git", [Map.class], null)
        binding.setVariable('currentBuild', new CurrentBuildMock())
        binding.setVariable('scm', null)
        binding.setVariable('docker', new DockerMock(helper))
        binding.setVariable('params', [
            'buildOnly': ''
        ])
        def env = new EnvMock(binding)
        env.JOB_NAME = 'apertis-v2021dev1/images/debos-image-build'
        env.WORKSPACE = '/srv/jenkins/workspace/debos-image-build-1'
        binding.setVariable('env', env)
    }

    @Test
    void should_execute_without_errors() throws Exception {
        runScript("Jenkinsfile")
        printCallStack()
        print '\n\n'
        assertTrue(helper.callStack.findAll { call ->
            call.methodName == "sh"
        }.any { call ->
            MethodCall.callArgsToString(call).contains("rsync")
        })
        assertJobStatusSuccess()
    }
}
