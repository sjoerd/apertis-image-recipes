{{- $architecture := or .architecture "amd64" }}
{{- $type := or .type "minimal" -}}
{{- $mirror := or .mirror "https://repositories.apertis.org/apertis/" -}}
{{- $suite := or .suite "v2022dev3" -}}
{{- $timestamp := or .timestamp "" -}}
{{- $snapshot := or .snapshot "" -}}
{{- $ospack := or .ospack (printf "ospack_%s-%s-%s" $suite $architecture $type) -}}
{{- $pack := or .pack "true" -}}
{{- $stable := or .stable "" -}}
{{- $osname := or .osname "apertis" -}}
{{- $keyring := or .keyring (printf "%s-archive-keyring" $osname) -}}

architecture: {{ $architecture }}

actions:
  - action: debootstrap
    suite: {{if eq $snapshot ""}} {{ $suite }} {{else}} {{ $suite }}/snapshots/{{ $snapshot }} {{end}}
    components:
      - target
    mirror: {{ $mirror }}
    variant: minbase
    keyring-package: {{ $keyring }}
    keyring-file: keyring/{{ $keyring }}.gpg
    merged-usr: true

  - action: overlay
    source: overlays/locale-default-c-utf8

  - action: overlay
    description: Work around "Hash Sum Mismatch" errors, https://phabricator.collabora.com/T15071
    source: overlays/apt-disable-http-pipelining

  # Add image version information
  - action: run
    description: "Setting up image version metadata"
    chroot: true
    script: scripts/setup_image_version.sh {{ $osname }} {{ $suite }} '{{ $timestamp }}' collabora {{ $type }}

  - action: run
    description: "Add extra apt sources"
    chroot: true
    script: scripts/apt_source.sh -m {{ $mirror }} -r {{ $suite }} {{if eq $stable "true"}} --updates --security {{end}} target {{if ne $snapshot ""}} --snapshot {{ $snapshot }} {{end}}

  - action: run
    description: "Add non-free apt source"
    chroot: true
    script: scripts/apt_source.sh -m {{ $mirror }} -r {{ $suite }} {{if eq $stable "true"}} --updates --security {{end}} non-free {{if ne $snapshot ""}} --snapshot {{ $snapshot }} {{end}}

  - action: overlay
    source: overlays/dpkg-exclusions

  - action: apt
    description: "Core packages"
    packages:
      - sudo
      - ca-certificates
      - initramfs-tools
      - libnss-myhostname
      - systemd-sysv

  - action: apt
    description: "Base packages"
    packages:
      - busybox
      - dbus-user-session

  - action: apt
    description: "Firmware packages"
    packages:
      - firmware-realtek
      - firmware-atheros

  - action: apt
    description: "Networking packages"
    packages:
      - busybox-ping
      - connman
      - iptables
      - netbase
      - wpasupplicant

  - action: apt
    description: "AppArmor packages"
    packages:
      - apparmor
      - chaiwala-apparmor-profiles

  - action: apt
    description: "Test environment packages"
    packages:
      - net-tools
      - openssh-client
      - openssh-server
      - vim.tiny

  - action: run
    description: Set the hostname
    chroot: false
    command: echo "{{ $osname }}" > "$ROOTDIR/etc/hostname"

  - action: overlay
    source: overlays/default-hosts

  - action: overlay
    source: overlays/iptables-persistence

  - action: overlay
    source: overlays/iptables-rules

  - action: overlay
    source: overlays/machine-info

  - action: overlay
    source: overlays/loopback-interface

  - action: overlay
    source: overlays/media-tmpfs

  - action: overlay
    source: overlays/create-homedir

  - action: overlay
    source: overlays/sudo-fqdn

  - action: overlay
    source: overlays/apertis-dev

  - action: overlay
    source: overlays/fsck

  - action: run
    chroot: true
    description: "Enable /tmp mount"
    script: scripts/enable-tmpfs.sh

  - action: run
    chroot: true
    script: scripts/add-xdg-user-metadata.sh

  - action: run
    chroot: true
    script: scripts/create-mtab-symlink.hook.sh

  - action: run
    chroot: true
    script: scripts/setup_user.sh

  - action: run
    chroot: true
    script: scripts/add_user_to_groups.sh

  - action: run
    chroot: true
    script: scripts/check_sudoers_for_admin.sh

  - action: run
    chroot: true
    script: scripts/generate_openssh_keys.sh

  - action: run
    chroot: true
    script: scripts/add-initramfs-modules.sh

  - action: run
    chroot: true
    description: "Disable daily apt download, upgrade and clean activities, https://phabricator.apertis.org/T6341"
    command: systemctl disable apt-daily.timer apt-daily-upgrade.timer

  - action: run
    chroot: true
    description: "Disable systemd-timesyncd service by default"
    command: systemctl disable systemd-timesyncd

  - action: run
    chroot: true
    description: "Disable e2scrub_all service by default"
    command: systemctl disable e2scrub_all

  - action: run
    chroot: true
    description: "Disable e2scrub_all timer by default"
    command: systemctl disable e2scrub_all.timer

  - action: run
    chroot: true
    description: "Disable e2scrub_reap service by default"
    command: systemctl disable e2scrub_reap

  - action: run
    chroot: true
    description: "Enable iptables services by default"
    command: systemctl enable iptables

{{- if eq $pack "true" }}
  - action: run
    description: "Save installed package status"
    chroot: false
    command: gzip -c "${ROOTDIR}/var/lib/dpkg/status" > "${ARTIFACTDIR}/{{ $ospack }}.pkglist.gz"

  - action: run
    description: List files on {{ $ospack }}
    chroot: false
    script: scripts/list-files "$ROOTDIR" | gzip > "${ARTIFACTDIR}/{{ $ospack }}.filelist.gz"

  - action: pack
    compression: gz
    file: {{ $ospack }}.tar.gz
{{- end }}
