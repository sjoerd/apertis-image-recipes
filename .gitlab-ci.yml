image: registry.gitlab.apertis.org/martyn/hacky-uml-docker/debos-uml-test

######
# variables
#
# The main variables to customize the built artifacts

variables:
  osname: apertis
  release: v2021dev1
  upload_host: archive@images.apertis.org
  upload_root: /srv/images/test/GITLABTESTS # /srv/images/public
  image_url_prefix: https://images.apertis.org/test/GITLABTESTS # https://images.apertis.org/
  ostree_path: ostree/repo/
  test_repo_url: https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}/pkg/development/apertis-tests.git
  demopack: https://images.apertis.org/media/multimedia-demo.tar.gz
  WORKSPACE: $CI_PROJECT_DIR
  collection_id: org.apertis.os

stages:
  - preparation
  - ospack build
  - ostree commit
  - installer image build
  - apt image build
  - ostree image build
  - ostree bundle build
  - ostree rollbackbundle build
  - sysroot build
  - artifacts upload
  - sysroot metadata upload
  - installer upload
  - ostree hawkbit upload
  - ostree push
  - submit tests

######
# templates
#
# The files included here contain the actual definitions of the jobs as templates
# that need to be instantiated with `extends` while setting the variables that
# control which ospack/hwpack to use.

include:
  - /.gitlab-ci/templates-ospack-build.yml
  - /.gitlab-ci/templates-ostree-commit.yml
  - /.gitlab-ci/templates-artifacts-build.yml
  - /.gitlab-ci/templates-upload.yml
  - /.gitlab-ci/templates-submit-tests.yml

######
# ospacks
#
# These variables are meant to be used when instantiating the templates
# included above to choose the desired ospack flavours

.minimal:
  variables:
    type: minimal

.target:
  variables:
    type: target

.basesdk:
  variables:
    type: basesdk
    debosarguments: "--scratchsize 10G"

.sdk:
  variables:
    type: sdk
    debosarguments: "--scratchsize 10G"

.devroot:
  variables:
    type: devroot

.sysroot:
  variables:
    type: sysroot

######
# hwpacks
#
# Another set of variables meant to be used when instantiating the templates
# included above, choosing the hwpack flavors

.amd64-uefi:
  variables:
    architecture: amd64
    board: uefi

.amd64-sdk:
  variables:
    architecture: amd64
    board: sdk

.armhf-uboot:
  variables:
    architecture: armhf
    board: uboot

.arm64-uboot:
  variables:
    architecture: arm64
    board: uboot

######
# stage: preparation

debos-dry-run:
  stage: preparation
  tags:
    - aws
  script:
    - 'for i in *.yaml ; do debos --print-recipe --dry-run "$i" ; done'

compute-version:
  stage: preparation
  tags:
    - aws
  script:
    - PIPELINE_VERSION=$(date +%Y%m%d.%H%M)
    - mkdir -p a/$PIPELINE_VERSION/meta
    - echo $PIPELINE_VERSION | tee a/$PIPELINE_VERSION/meta/build-id.txt
    - echo $CI_COMMIT_SHA | tee a/$PIPELINE_VERSION/meta/commit.txt
    - echo $CI_PIPELINE_URL | tee a/$PIPELINE_VERSION/meta/pipeline-url.txt
  artifacts:
    expire_in: 1d
    paths:
      - a/*/meta/*

######
# stage: ospack build

ospack-build-amd64-minimal:
  extends:
    - .ospack-build
    - .amd64-uefi
    - .minimal

ospack-build-armhf-minimal:
  extends:
    - .ospack-build
    - .armhf-uboot
    - .minimal

ospack-build-arm64-minimal:
  extends:
    - .ospack-build
    - .arm64-uboot
    - .minimal

ospack-build-amd64-target:
  extends:
    - .ospack-build
    - .amd64-uefi
    - .target

ospack-build-armhf-target:
  extends:
    - .ospack-build
    - .armhf-uboot
    - .target

ospack-build-amd64-basesdk:
  timeout: 3h
  extends:
    - .ospack-build
    - .amd64-sdk
    - .basesdk

ospack-build-amd64-sdk:
  timeout: 3h
  extends:
    - .ospack-build
    - .amd64-sdk
    - .sdk

ospack-build-amd64-devroot:
  extends:
    - .ospack-build
    - .amd64-uefi
    - .devroot

ospack-build-armhf-devroot:
  extends:
    - .ospack-build
    - .armhf-uboot
    - .devroot

ospack-build-arm64-devroot:
  extends:
    - .ospack-build
    - .arm64-uboot
    - .devroot

ospack-build-amd64-sysroot:
  extends:
    - .ospack-build
    - .amd64-uefi
    - .sysroot

ospack-build-armhf-sysroot:
  extends:
    - .ospack-build
    - .armhf-uboot
    - .sysroot

ospack-build-arm64-sysroot:
  extends:
    - .ospack-build
    - .arm64-uboot
    - .sysroot

######
# stage: ostree commit

ostree-commit-amd64-minimal-uefi:
  timeout: 2h
  extends:
    - .ostree-commit
    - .amd64-uefi
    - .minimal
  needs:
    - compute-version
    - ospack-build-amd64-minimal

ostree-commit-armhf-minimal-uboot:
  timeout: 2h
  extends:
    - .ostree-commit
    - .armhf-uboot
    - .minimal
  needs:
    - compute-version
    - ospack-build-armhf-minimal

ostree-commit-arm64-minimal-uboot:
  timeout: 2h
  extends:
    - .ostree-commit
    - .arm64-uboot
    - .minimal
  needs:
    - compute-version
    - ospack-build-arm64-minimal

ostree-commit-amd64-target-uefi:
  timeout: 2h
  extends:
    - .ostree-commit
    - .amd64-uefi
    - .target
  needs:
    - compute-version
    - ospack-build-amd64-target

ostree-commit-armhf-target-uboot:
  timeout: 2h
  extends:
    - .ostree-commit
    - .armhf-uboot
    - .target
  needs:
    - compute-version
    - ospack-build-armhf-target

######
# stage: artifacts build

sysroot-build-amd64-sysroot:
  variables:
    debosarguments: --scratchsize 10G
  extends:
    - .sysroot-build
    - .amd64-uefi
    - .sysroot
  needs:
    - compute-version
    - ospack-build-amd64-sysroot

sysroot-build-armhf-sysroot:
  variables:
    debosarguments: --scratchsize 10G
  extends:
    - .sysroot-build
    - .armhf-uboot
    - .sysroot
  needs:
    - compute-version
    - ospack-build-armhf-sysroot

sysroot-build-arm64-sysroot:
  variables:
    debosarguments: --scratchsize 10G
  extends:
    - .sysroot-build
    - .arm64-uboot
    - .sysroot
  needs:
    - compute-version
    - ospack-build-arm64-sysroot

ostree-rollbackbundle-build-amd64-minimal-uefi:
  extends:
    - .ostree-rollbackbundle-build
    - .amd64-uefi
    - .minimal
  needs:
    - compute-version
    - ostree-commit-amd64-minimal-uefi

ostree-rollbackbundle-build-armhf-minimal-uboot:
  extends:
    - .ostree-rollbackbundle-build
    - .armhf-uboot
    - .minimal
  needs:
    - compute-version
    - ostree-commit-armhf-minimal-uboot

ostree-rollbackbundle-build-arm64-minimal-uboot:
  extends:
    - .ostree-rollbackbundle-build
    - .arm64-uboot
    - .minimal
  needs:
    - compute-version
    - ostree-commit-arm64-minimal-uboot

ostree-rollbackbundle-build-amd64-target-uefi:
  extends:
    - .ostree-rollbackbundle-build
    - .amd64-uefi
    - .target
  needs:
    - compute-version
    - ostree-commit-amd64-target-uefi

ostree-rollbackbundle-build-armhf-target-uboot:
  extends:
    - .ostree-rollbackbundle-build
    - .armhf-uboot
    - .target
  needs:
    - compute-version
    - ostree-commit-armhf-target-uboot

ostree-bundle-build-amd64-minimal-uefi:
  extends:
    - .ostree-bundle-build
    - .amd64-uefi
    - .minimal
  needs:
    - compute-version
    - ostree-commit-amd64-minimal-uefi

ostree-bundle-build-armhf-minimal-uboot:
  extends:
    - .ostree-bundle-build
    - .armhf-uboot
    - .minimal
  needs:
    - compute-version
    - ostree-commit-armhf-minimal-uboot

ostree-bundle-build-arm64-minimal-uboot:
  extends:
    - .ostree-bundle-build
    - .arm64-uboot
    - .minimal
  needs:
    - compute-version
    - ostree-commit-arm64-minimal-uboot

ostree-bundle-build-amd64-target-uefi:
  extends:
    - .ostree-bundle-build
    - .amd64-uefi
    - .target
  needs:
    - compute-version
    - ostree-commit-amd64-target-uefi

ostree-bundle-build-armhf-target-uboot:
  extends:
    - .ostree-bundle-build
    - .armhf-uboot
    - .target
  needs:
    - compute-version
    - ostree-commit-armhf-target-uboot

ostree-image-build-amd64-minimal-uefi:
  extends:
    - .ostree-image-build
    - .amd64-uefi
    - .minimal
  needs:
    - compute-version
    - ostree-commit-amd64-minimal-uefi

ostree-image-build-armhf-minimal-uboot:
  extends:
    - .ostree-image-build
    - .armhf-uboot
    - .minimal
  needs:
    - compute-version
    - ostree-commit-armhf-minimal-uboot

ostree-image-build-arm64-minimal-uboot:
  extends:
    - .ostree-image-build
    - .arm64-uboot
    - .minimal
  needs:
    - compute-version
    - ostree-commit-arm64-minimal-uboot

ostree-image-build-amd64-target-uefi:
  extends:
    - .ostree-image-build
    - .amd64-uefi
    - .target
  needs:
    - compute-version
    - ostree-commit-amd64-target-uefi

ostree-image-build-armhf-target-uboot:
  extends:
    - .ostree-image-build
    - .armhf-uboot
    - .target
  needs:
    - compute-version
    - ostree-commit-armhf-target-uboot

apt-image-build-amd64-minimal-uefi:
  extends:
    - .apt-image-build
    - .amd64-uefi
    - .minimal
  needs:
    - compute-version
    - ospack-build-amd64-minimal

apt-image-build-armhf-minimal-uboot:
  extends:
    - .apt-image-build
    - .armhf-uboot
    - .minimal
  needs:
    - compute-version
    - ospack-build-armhf-minimal

apt-image-build-arm64-minimal-uboot:
  extends:
    - .apt-image-build
    - .arm64-uboot
    - .minimal
  needs:
    - compute-version
    - ospack-build-arm64-minimal

apt-image-build-amd64-target-uefi:
  extends:
    - .apt-image-build
    - .amd64-uefi
    - .target
  needs:
    - compute-version
    - ospack-build-amd64-target

apt-image-build-armhf-target-uboot:
  extends:
    - .apt-image-build
    - .armhf-uboot
    - .target
  needs:
    - compute-version
    - ospack-build-armhf-target

apt-image-build-amd64-basesdk-sdk:
  timeout: 2h
  extends:
    - .apt-image-build
    - .amd64-sdk
    - .basesdk
  before_script:
    - PIPELINE_VERSION=$(cat a/*/meta/build-id.txt)
    - devrootpack=../../armhf/devroot/ospack_${release}-armhf-devroot_${PIPELINE_VERSION}.tar.gz
    - debosarguments="
      -t devrootpack:${devrootpack}
      --scratchsize 10G"
  needs:
    - compute-version
    - ospack-build-amd64-basesdk
    - ospack-build-armhf-devroot

apt-image-build-amd64-sdk-sdk:
  timeout: 2h
  extends:
    - .apt-image-build
    - .amd64-sdk
    - .sdk
  before_script:
    - PIPELINE_VERSION=$(cat a/*/meta/build-id.txt)
    - devrootpack=../../armhf/devroot/ospack_${release}-armhf-devroot_${PIPELINE_VERSION}.tar.gz
    - debosarguments="
      -t demopack:${demopack}
      -t sampleappscheckout:enabled
      -t devrootpack:${devrootpack}
      --scratchsize 10G"
  needs:
    - compute-version
    - ospack-build-amd64-sdk
    - ospack-build-armhf-devroot

installer-image-build-mx6qsabrelite-uboot:
  extends:
    - .installer-image-build
  variables:
    target: 'mx6qsabrelite-uboot'

######
# stage: upload

artifacts-upload-amd64-minimal-uefi:
  extends:
    - .artifacts-upload
    - .amd64-uefi
    - .minimal
  needs:
    - compute-version
    - ospack-build-amd64-minimal
    - ostree-bundle-build-amd64-minimal-uefi
    - ostree-rollbackbundle-build-amd64-minimal-uefi
    - ostree-image-build-amd64-minimal-uefi
    - apt-image-build-amd64-minimal-uefi

artifacts-upload-armhf-minimal-uboot:
  extends:
    - .artifacts-upload
    - .armhf-uboot
    - .minimal
  needs:
    - compute-version
    - ospack-build-armhf-minimal
    - ostree-bundle-build-armhf-minimal-uboot
    - ostree-rollbackbundle-build-armhf-minimal-uboot
    - ostree-image-build-armhf-minimal-uboot
    - apt-image-build-armhf-minimal-uboot

artifacts-upload-arm64-minimal-uboot:
  extends:
    - .artifacts-upload
    - .arm64-uboot
    - .minimal
  needs:
    - compute-version
    - ospack-build-arm64-minimal
    - ostree-bundle-build-arm64-minimal-uboot
    - ostree-rollbackbundle-build-arm64-minimal-uboot
    - ostree-image-build-arm64-minimal-uboot
    - apt-image-build-arm64-minimal-uboot

artifacts-upload-amd64-target-uefi:
  extends:
    - .artifacts-upload
    - .amd64-uefi
    - .target
  needs:
    - compute-version
    - ospack-build-amd64-target
    - ostree-bundle-build-amd64-target-uefi
    - ostree-rollbackbundle-build-amd64-target-uefi
    - ostree-image-build-amd64-target-uefi
    - apt-image-build-amd64-target-uefi

artifacts-upload-armhf-target-uboot:
  extends:
    - .artifacts-upload
    - .armhf-uboot
    - .target
  needs:
    - compute-version
    - ospack-build-armhf-target
    - ostree-bundle-build-armhf-target-uboot
    - ostree-rollbackbundle-build-armhf-target-uboot
    - ostree-image-build-armhf-target-uboot
    - apt-image-build-armhf-target-uboot

artifacts-upload-amd64-basesdk-sdk:
  extends:
    - .artifacts-upload
    - .amd64-sdk
    - .basesdk
  needs:
    - compute-version
    - ospack-build-amd64-basesdk
    - apt-image-build-amd64-basesdk-sdk

artifacts-upload-amd64-sdk-sdk:
  extends:
    - .artifacts-upload
    - .amd64-sdk
    - .basesdk
  needs:
    - compute-version
    - ospack-build-amd64-sdk
    - apt-image-build-amd64-sdk-sdk

artifacts-upload-amd64-syroot:
  extends:
    - .artifacts-upload
    - .amd64-uefi
    - .sysroot
  needs:
    - compute-version
    - ospack-build-amd64-sysroot
    - sysroot-build-amd64-sysroot

ostree-push-amd64-minimal-uefi:
  extends:
    - .ostree-push
    - .amd64-uefi
    - .minimal
  needs:
    - compute-version
    - ostree-commit-amd64-minimal-uefi

ostree-push-armhf-minimal-uboot:
  extends:
    - .ostree-push
    - .armhf-uboot
    - .minimal
  needs:
    - compute-version
    - ostree-commit-armhf-minimal-uboot

ostree-push-arm64-minimal-uboot:
  extends:
    - .ostree-push
    - .arm64-uboot
    - .minimal
  needs:
    - compute-version
    - ostree-commit-arm64-minimal-uboot

ostree-push-amd64-target-uefi:
  extends:
    - .ostree-push
    - .amd64-uefi
    - .target
  needs:
    - compute-version
    - ostree-commit-amd64-target-uefi

ostree-push-armhf-target-uboot:
  extends:
    - .ostree-push
    - .armhf-uboot
    - .target
  needs:
    - compute-version
    - ostree-commit-armhf-target-uboot

ostree-hawkbit-upload-amd64-minimal-uefi:
  extends:
    - .ostree-hawkbit-upload
    - .amd64-uefi
    - .minimal
  needs:
    - compute-version
    - ostree-bundle-build-amd64-minimal-uefi

ostree-hawkbit-upload-armhf-minimal-uboot:
  extends:
    - .ostree-hawkbit-upload
    - .armhf-uboot
    - .minimal
  needs:
    - compute-version
    - ostree-bundle-build-armhf-minimal-uboot

ostree-hawkbit-upload-arm64-minimal-uboot:
  extends:
    - .ostree-hawkbit-upload
    - .arm64-uboot
    - .minimal
  needs:
    - compute-version
    - ostree-bundle-build-arm64-minimal-uboot

ostree-hawkbit-upload-amd64-target-uefi:
  extends:
    - .ostree-hawkbit-upload
    - .amd64-uefi
    - .target
  needs:
    - compute-version
    - ostree-bundle-build-amd64-target-uefi

ostree-hawkbit-upload-armhf-target-uboot:
  extends:
    - .ostree-hawkbit-upload
    - .armhf-uboot
    - .target
  needs:
    - compute-version
    - ostree-bundle-build-armhf-target-uboot

sysroot-metadata-upload-amd64-sysroot:
  extends:
    - .sysroot-metadata-upload
    - .amd64-uefi
    - .sysroot
  needs:
    - compute-version
    - sysroot-build-amd64-sysroot

sysroot-metadata-upload-armhf-sysroot:
  extends:
    - .sysroot-metadata-upload
    - .armhf-uboot
    - .sysroot
  needs:
    - compute-version
    - sysroot-build-armhf-sysroot

sysroot-metadata-upload-arm64-sysroot:
  extends:
    - .sysroot-metadata-upload
    - .arm64-uboot
    - .sysroot
  needs:
    - compute-version
    - sysroot-build-arm64-sysroot

installer-upload:
  extends: .installer-upload

######
# stage: submit tests

submit-tests-apt-amd64-minimal-uefi:
  extends:
    - .submit-tests-apt
    - .amd64-uefi
    - .minimal
  needs:
    - compute-version
    - artifacts-upload-amd64-minimal-uefi

submit-tests-apt-armhf-minimal-uboot:
  extends:
    - .submit-tests-apt
    - .armhf-uboot
    - .minimal
  needs:
    - compute-version
    - artifacts-upload-armhf-minimal-uboot

submit-tests-apt-arm64-minimal-uboot:
  extends:
    - .submit-tests-apt
    - .arm64-uboot
    - .minimal
  needs:
    - compute-version
    - artifacts-upload-arm64-minimal-uboot

submit-tests-apt-amd64-target-uefi:
  extends:
    - .submit-tests-apt
    - .amd64-uefi
    - .target
  needs:
    - compute-version
    - artifacts-upload-amd64-target-uefi

submit-tests-apt-armhf-target-uboot:
  extends:
    - .submit-tests-apt
    - .armhf-uboot
    - .target
  needs:
    - compute-version
    - artifacts-upload-armhf-target-uboot

submit-tests-apt-amd64-basesdk-sdk:
  extends:
    - .submit-tests-apt
    - .amd64-sdk
    - .basesdk
  needs:
    - compute-version
    - artifacts-upload-amd64-basesdk-sdk

submit-tests-apt-amd64-sdk-sdk:
  extends:
    - .submit-tests-apt
    - .amd64-sdk
    - .sdk
  needs:
    - compute-version
    - artifacts-upload-amd64-sdk-sdk

submit-tests-ostree-amd64-minimal-uefi:
  extends:
    - .submit-tests-ostree
    - .amd64-uefi
    - .minimal
  needs:
    - compute-version
    - artifacts-upload-amd64-minimal-uefi
    - ostree-push-amd64-minimal-uefi

submit-tests-ostree-armhf-minimal-uboot:
  extends:
    - .submit-tests-ostree
    - .armhf-uboot
    - .minimal
  needs:
    - compute-version
    - artifacts-upload-armhf-minimal-uboot
    - ostree-push-armhf-minimal-uboot

submit-tests-ostree-arm64-minimal-uboot:
  extends:
    - .submit-tests-ostree
    - .arm64-uboot
    - .minimal
  needs:
    - compute-version
    - artifacts-upload-arm64-minimal-uboot
    - ostree-push-arm64-minimal-uboot

submit-tests-ostree-amd64-target-uefi:
  extends:
    - .submit-tests-ostree
    - .amd64-uefi
    - .target
  needs:
    - compute-version
    - artifacts-upload-amd64-target-uefi
    - ostree-push-amd64-target-uefi

submit-tests-ostree-armhf-target-uboot:
  extends:
    - .submit-tests-ostree
    - .armhf-uboot
    - .target
  needs:
    - compute-version
    - artifacts-upload-armhf-target-uboot
    - ostree-push-armhf-target-uboot
