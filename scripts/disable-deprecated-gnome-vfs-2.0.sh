#!/bin/sh

set -e

echo "[org.gnome.devhelp.state.main.contents]
books-disabled=['gnome-vfs-2.0']" > \
	/usr/share/glib-2.0/schemas/devhelp-disable-gnome-vfs.gschema.override

if ! glib-compile-schemas /usr/share/glib-2.0/schemas
then
        echo "Failed to compile glib schemas"
        exit 1
fi

