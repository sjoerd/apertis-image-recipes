#!/bin/sh

env

set -eu

if [ -z  "${ROOTDIR}" ] ; then
  echo "ROOTDIR not given"
  exit 1
fi

kernel=${ROOTDIR}/vmlinuz
initramfs=${ROOTDIR}/initrd.img
dtb="$(find ${ROOTDIR}/boot/dtbs -name imx6q-sabrelite.dtb)"

SIGNDIR=${RECIPEDIR}/sign/imx6

if [ ! -L "${kernel}" ]; then
    echo "Kernel not found"
    exit 1
fi

target="$(readlink ${kernel})"

# Need to change dir for CST
cd ${SIGNDIR}

${RECIPEDIR}/scripts/generate_signed_fit_image.py \
    -k "${kernel}" \
    -r "${initramfs}" \
    -d "${dtb}" \
    -t fit_image.template \
    -s fit_image_csf.template \
    -o "${ROOTDIR}/${target}".itb

rm -fr "${kernel}" "${initramfs}" "${ROOTDIR}/${target}" "${ROOTDIR}/${target}".itb.bin "${ROOTDIR}"/boot/dtbs "${ROOTDIR}"/boot/initr*

ln -sf "${target}".itb "${kernel}"

ls -l "${kernel}" "${ROOTDIR}"/boot/
