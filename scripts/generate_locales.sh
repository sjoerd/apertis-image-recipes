#!/bin/sh

set -e

# Generate locales listed at /var/lib/locales/supported.d/local
# Supported locales listed at /usr/share/i18n/SUPPORTED

echo "I: Regenerate locales"
mkdir -p /var/lib/locales/supported.d/
cat >/var/lib/locales/supported.d/local<<'EOF'
ar_QA.UTF-8 UTF-8
ar_SA.UTF-8 UTF-8
cs_CZ.UTF-8 UTF-8
da_DK.UTF-8 UTF-8
de_DE.UTF-8 UTF-8
el_GR.UTF-8 UTF-8
en_GB.UTF-8 UTF-8
en_US.UTF-8 UTF-8
es_AR.UTF-8 UTF-8
es_ES.UTF-8 UTF-8
es_MX.UTF-8 UTF-8
fi_FI.UTF-8 UTF-8
fr_CA.UTF-8 UTF-8
fr_FR.UTF-8 UTF-8
hu_HU.UTF-8 UTF-8
id_ID.UTF-8 UTF-8
it_IT.UTF-8 UTF-8
ms_MY.UTF-8 UTF-8
nb_NO.UTF-8 UTF-8
nl_NL.UTF-8 UTF-8
pl_PL.UTF-8 UTF-8
pt_BR.UTF-8 UTF-8
pt_PT.UTF-8 UTF-8
ro_RO.UTF-8 UTF-8
ru_RU.UTF-8 UTF-8
sk_SK.UTF-8 UTF-8
sv_SE.UTF-8 UTF-8
th_TH.UTF-8 UTF-8
tr_TR.UTF-8 UTF-8
zh_CN.UTF-8 UTF-8
zh_HK.UTF-8 UTF-8
EOF

if [ -x /usr/sbin/locale-gen ]
then
	locale-gen
else
	echo "E: Failed to setup locales. No locale-gen detected."
	echo "E: Proceeding... non fatal error."
fi
