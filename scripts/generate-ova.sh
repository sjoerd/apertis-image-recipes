#!/bin/sh

set -e

OVF_TEMPLATE=${RECIPEDIR}/scripts/vm-template.ovf

usage() {
	echo "$0 [OPTIONS] <disk.img>
	Generate OVA image based on raw disk image file.

	options:
	 -h           Display this help and exit
	 -o file.ova  Write output to OVA file. Default: filename from input <disk.img> file
	 -n name      Virtual machine name. Default: filename from input <disk.img> file
	"
	exit 1
}

gen_uuid() {
	# Generate a random UUID.
	cat /proc/sys/kernel/random/uuid
}

path_to_filename() {
	# Extract filename (without extension) from the full path of a file.
	fullpath=$1
	fullfilename=$(basename -- "$fullpath")
	echo "${fullfilename%.*}"
}

generate_ova() {
	ovf=$filename.ovf
	vmdk=$filename.vmdk
	vmdk_gz=$vmdk.gz

	disk_uuid="$(gen_uuid)"
	machine_uuid="$(gen_uuid)"

	# VM disk file is vmdk format compressed with gzip
	qemu-img convert -O vmdk $diskimg $vmdk
	gzip $vmdk

	cat $OVF_TEMPLATE |
		sed "s/%DiskFile%/$vmdk_gz/g" |
		sed "s/%DiskUUID%/$disk_uuid/g" |
		sed "s/%MachineName%/$name/g" |
		sed "s/%MachineUUID%/$machine_uuid/g" |
		sed "s/%VirtualSystemId%/$filename/g" |
		sed "s/%VirtualSystemIdentifier%/$filename/g" > $ovf

	tar -cf $ova $ovf $vmdk_gz
	rm -f $ovf $vmdk_gz
}

while getopts ":ho:n:" opt; do
	case $opt in
	h)
		usage
		;;
	n)
		name=$OPTARG
		;;
	o)
		ova=$OPTARG
		;;
	\?)
		echo Error: Unknown option: -$OPTARG >&2
		echo >&2
		usage
		exit 1
		;;
	esac
done
shift $((OPTIND -1))

if [ $# -lt 1 ]; then
	echo Error: Need disk image file. >&2
	echo >&2
	usage
	exit 1
fi

diskimg=$1
shift 1

if [ ! -f $diskimg ]; then
	echo "File $diskimg not found!" >&2
	echo >&2
	usage
	exit 1
fi

# Extract filename from input disk image. This is used as a default value for
# temporary files and undefined parameters.
filename=$(path_to_filename $diskimg)

if [ -z "$name" ]; then
	name=$filename
	echo "Using default Virtual machine name: $name"
fi

if [ -z "$ova" ]; then
	ova=$filename.ova
	echo "Using default output OVA file: $ova"
fi

generate_ova
