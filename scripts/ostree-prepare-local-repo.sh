#!/bin/sh

set -eu

if [ $# -ne 3 ]
then
  echo "Usage: $0 REPO PULL_URL BRANCH" >&2
  exit 1
fi

repo=$1
ostree_pull_url=$2
branch=$3

mkdir -p "${repo}"
ostree init --repo="${repo}" --mode archive-z2
ostree remote --repo="${repo}" add --no-gpg-verify origin "${ostree_pull_url}"

echo Pulling from "${ostree_pull_url}/refs/heads/${branch}"
http_code=$(curl --location --silent -o /dev/null --head -w "%{http_code}" "${ostree_pull_url}/refs/heads/${branch}")
case "$http_code" in
  200)
    ostree pull --repo="${repo}" --depth=-1 --mirror --disable-fsync origin "${branch}"
    ostree pull --repo="${repo}" --depth=-1 --mirror --disable-fsync origin "${branch}"
    ostree show --repo="${repo}" "${branch}"
    ;;
  404)
    echo "No remote ${branch} branch found"
    ;;
  *)
    echo "Error: Got HTTP '$http_code' trying to fetch '${ostree_pull_url}/refs/heads/${branch}'"
    exit 1
    ;;
esac
