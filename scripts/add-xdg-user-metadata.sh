#!/bin/sh

set -e

# Hackish: Add user XDG metadata BUG: #1157
echo "I: Adding XDG user metadata."
for d in .config .cache .dbus
do
    mkdir -p "/etc/skel/$d"
    mkdir -p "/root/$d"
done

for d in bin etc include lib libexec share
do
    mkdir -p "/etc/skel/.local/$d"
    mkdir -p "/root/.local/$d"
done
