Apertis Image Recipes
=====================

This repository contains the recipes used to build the Apertis reference images.

Getting started
===============

To experiment with Apertis, begin with the `apertis-sample-*.yaml` recipes.
They are a good starting point for building your own images based on the
reference Apertis recipes.

From the Apertis SDK, build your first image:

    $ sudo debos apertis-sample-image-development.yaml

The `apertis-sample-image-development.yaml` recipe builds an image suitable for
the ARM-based i.MX6 SABRE Lite board with all the development tools installed.

Copy it to a SD card and then plug the SD card in your board to boot Apertis
on it.

The image can be copied to the SD card by decompressing it and then copying the
raw bytes using tools like `dd`.

If the SD Card reader is available from the Apertis SDK, the
[`bmaptool`](https://github.com/intel/bmap-tools) command provides an
alternative that is faster, more reliable and easy to use:

    $ sudo bmaptool copy apertis-sample-development-*-minimal-armhf.img.gz $SDCARD

You can now connect to your board via a serial connection, the default
credentials are `user:user`.

Depending on your network setup, your board may also be available via SSH with
the same credentials:

    $ ssh user@apertis.local

With the development image you can install, remove, and update packages using
the standard Debian tools like `apt`, including the GPLv3 components that are
excluded by default from the Apertis image:

    $ sudo apt install gdbserver

You can edit the recipe to customize the package selection, run custom scripts
to change the rootfs contents, and download contents from remote sources.

For images to be deployed in production, Apertis uses OSTree to manage updates
in a reliable way.

The `apertis-sample-image-production.yaml` recipe builds an image that boots
into an OSTree deployment with the ability to automatically roll-back updates
in case of boot failures:

    $ sudo debos apertis-sample-image-production.yaml
    $ sudo bmaptool copy apertis-sample-production-*-minimal-armhf.img.gz $SDCARD

Providing a reproducible development environment is one of the key elements for
Apertis, and the `apertis-sample-image-sdk.yaml` gives you the ability to
generate your own SDK images, bootable in VirtualBox:

    $ sudo debos --scratchsize 10G apertis-sample-image-sdk.yaml

You can then pick the generated VDI file and
[set it up in VirtualBox](https://wiki.apertis.org/VirtualBox).

Customizing the recipes
=======================

Open the sample recipes and add, remove or edit their actions to make the
generated artifacts suit your needs.

Some customization examples are already provided, check the Debos documentation
for a full list of the available actions:

https://godoc.org/github.com/go-debos/debos/actions

The reference recipes
=====================

The main `Jenkinsfile` orchestrates how platform-independent ospacks and
platform-specific hwpacks are combined to generate all the reference artifacts.

Architectures supported via `-t architecture:$VALUE`:
  * arm64: (uboot) currently targets ARM 64bit Renesas R-Car boards
  * amd64: (uefi) supports any UEFI x86-64bit system
  * armhf: (uboot) currently targets the ARM 32bit i.MX6 SABRE Lite

Package set selection via `-t type:$VALUE`:
  * minimal: headless system
  * target: Mildenhall HMI
  * sdk: XFCE-based development environment with the Mildenhall HMI tools
  * basesdk: basic XFCE-based development environment
  * sysroot: cross-compilation target tarballs
  * devroot: emulation-based foreign platform build environments

Deployment types:
  * APT: suitable for development, prioritizes flexibility over robustness
  * OSTree: suitable for production, prioritizes robustness over flexibility

Parameters for ospack and image versioning:
  * suite: `-t suite:v2021dev1`
  * timestamp: `-t timestamp:$(date +%s)`

Building in Docker
==================

To build in a reproducible environment outside of the Apertis SDK, using the
pre-built Docker images in the Apertis registry is recommended:

    $ RELEASE=v2021dev1 # the Apertis release to be built
    $ docker run \
      -i -t \
      -u $(id -u) \
      --group-add=$(getent group kvm | cut -d : -f 3) \
      --device /dev/kvm \
      -w /recipes \
      -v $(pwd):/recipes \
      docker-registry.apertis.org/apertis/apertis-$RELEASE-image-builder \
      debos apertis-ospack-minimal.yaml

The image build will be run in the Docker container using your uid with the
host's kvm group to ensure debos/fakemachine can use hardware virtualisation.
